##################################################
### Variables                                  ###
##################################################
IMAGES = $(wildcard images/*)
CI_REGISTRY_IMAGE ?= registry.gitlab.com/altahrim/yabudo

SHELL = /bin/bash
.SHELLFLAGS = -c


##################################################
### Build docker images                        ###
##################################################
build: $(addprefix build/,$(IMAGES))
	echo $(IMAGES)

# Files keep tracks of latests builds.
build/%: %/Dockerfile %/VERSION
	$(call build_image,$(@),$(<D))

build/images/shunit: images/shunit/Dockerfile images/shunit/VERSION build/images/make
	$(call build_image,$(@),$(<D))

define build_image
	@printf '\e[1m# \e[32mBuild image %s…\e[0m\n' $(shell basename '$(1)')
	docker build --build-arg VERSION='$(shell cat '$(2)/VERSION')' --compress -t '$(CI_REGISTRY_IMAGE)/$(@F):$(shell cat '$(2)/VERSION')' '$(2)'
	@mkdir -m 777 -vp '$(shell dirname '$(1)')'
	touch '$(1)'
endef

clean:
	rm -vrf build push


##################################################
### Push on registry                           ###
##################################################
push: $(addprefix push/,$(IMAGES))

# Files keeps tracks of latests pushs.
push/%: build/%
	@printf '# \e[1;32mPush image %s on registry…\e[0m\n' $(@F)
	docker push '$(CI_REGISTRY_IMAGE)/$(@F):$(shell cat 'images/$(@F)/VERSION')'
	@mkdir -m 777 -vp '$(@D)'
	touch '$(@)'


##################################################
### Tests                                      ###
##################################################
lint: hadolint

hadolint:
	@printf "\\e[48;5;24;1m %-80s\\e[0m\\n" "Launch Hadolint tests…"
	$(MAKE) -k $(addsuffix /check_haolint,$(shell find images -name "Dockerfile"))
	printf "\\e[48;5;28;1m %-80s\\e[0m\\n" "Hadolint checks OK!"

%/check_haolint:
	@set -o pipefail; \
	if ! git check-ignore -q '$(@D)'; then \
	  printf '\e[38;5;250mHadolint: %s \e[0m\n' '$(@D)'; \
	  hadolint '$(@D)' | xargs -I{} printf "\e[33m%s\e[0m\n" '{}'; \
	fi

test:
	# TODO Check if bin version match expected version for each image


##################################################
### Help                                       ###
##################################################
help:
	echo 'Set of docker images for various projects.'
	echo
	echo 'Here are some commands you can use: '
	echo '– build    : Build docker images'
	echo '– push     : Push docker images on configured registry. You must login first.'
	echo '– lint     : Run some linters on source'
	echo '– hadolint : Run hadolint on all Dockerfile'
	echo '– clean    : Remove generated files'
	echo


##################################################
.PHONY: build push lint hadolint help
.SILENT: clean hadolint help
